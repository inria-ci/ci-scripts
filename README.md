# CI scripts

This public repository contains various scripts that the CI service
use to administrate `ci.inria.fr`.

These scripts are public for code review and can be used as examples
of usage of the CloudStack API.

## Available scripts:

- `clean_stopped_macos_vm`: this script is used to detect stopped
  MacOS VMs and downscale them to minimal instances to save RAM.

- `vm_expiration`: this script is run daily to check VM expiration
  dates.
