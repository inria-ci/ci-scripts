"""scale stopped Mac OS X to minimal offering"""

import email.mime.text
import itertools
import logging
import smtplib
import textwrap

import cs

DRY_RUN = True

EMAIL_FROM = "ci-staff@inria.fr"

EMAIL = {
    "subject": "CI project {domain}: stopped Mac OS VMs have been downscaled",
    "body": textwrap.dedent(
        """
        Dear CI user,

        The following Mac OS VMs in CI project {domain} were stopped and have
        been downscaled to the minimal offering (CPU 1024MHz & RAM 1024MB):
        {vms}

        All data have been preserved. Before restarting some of these machines,
        you should restore their original offering with CloudStack:
        https://sesi-cloud-ctl1.inria.fr/client/

        Cheers,
        --
        The CI service.
        """
    ),
}


def get_offering_id(cloudstack, cpuspeed, memory) -> str:
    """return the id of the computer offering with a given CPU speed (MHz)
    and memory (MB)"""
    offerings = cloudstack.listServiceOfferings()["serviceoffering"]
    return next(
        offering["id"]
        for offering in offerings
        if offering.get("cpuspeed", None) == cpuspeed
        and offering.get("memory", None) == memory
    )


def describe_machine(machine) -> str:
    """return readable machine description"""
    displayname = machine["displayname"]
    cpuspeed = machine["cpuspeed"]
    memory = machine["memory"]
    return f"{displayname} (original CPU: {cpuspeed}MHz, RAM: {memory}MB)"


def notify_by_mail(cloudstack, domain, domainid, machines) -> None:
    """send scale notification"""
    vms = "\n".join(f"- {describe_machine(machine)}" for machine in machines)
    variables = {"vms": vms, "domain": domain}
    subject = EMAIL["subject"].format(**variables)
    body = EMAIL["body"].format(**variables)
    users = cloudstack.listUsers(domainid=domainid)["user"]
    recipients = [user["email"] for user in users]
    msg = email.mime.text.MIMEText(body, "plain", "utf-8")
    msg["Subject"] = subject
    msg["To"] = ", ".join(recipients)
    msg["From"] = EMAIL_FROM
    smtp = smtplib.SMTP(host="smtp.inria.fr", port=25)
    smtp.ehlo()
    smtp.send_message(msg, EMAIL_FROM, recipients)


def execute() -> None:
    """scale all scalable machines"""
    cloudstack = cs.CloudStack(**cs.read_config())
    minimal_offering_id = get_offering_id(cloudstack, 1024, 1024)
    logging.info("Minimal offering ID: %s", minimal_offering_id)
    machines = cloudstack.listVirtualMachines(
        hypervisor="VMware", listall=True, state="Stopped"
    )["virtualmachine"]
    by_domains = itertools.groupby(machines, lambda machine: machine["domain"])
    gain = 0
    for domain, machines in by_domains:
        scalable_machines = [
            machine
            for machine in machines
            if machine["cpuspeed"] != 1024 or machine["memory"] != 1024
        ]
        if scalable_machines != []:
            logging.info(
                "Stopped machines in domain %s: %s",
                domain,
                ", ".join(
                    f"{machine['displayname']} ({describe_machine(machine)})"
                    for machine in scalable_machines
                ),
            )
            gain += sum(
                machine["memory"] - 1024 for machine in scalable_machines
            )
            if not DRY_RUN:
                notify_by_mail(
                    cloudstack,
                    domain,
                    scalable_machines[0]["domainid"],
                    scalable_machines,
                )
                for machine in scalable_machines:
                    cloudstack.scaleVirtualMachine(
                        id=machine["id"], serviceofferingid=minimal_offering_id
                    )
    logging.info("RAM gain: %dMB", gain)


logging.basicConfig(level="INFO")
execute()
