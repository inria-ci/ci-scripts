import collections
import email.mime.text
import logging
import smtplib

import cs

logging.basicConfig(level=logging.INFO)

EMAIL_FROM = "ci-staff@inria.fr"


def main(cloudstack):
    host = next(
        iter(
            cloudstack.listHosts(
                name="pro30-1-10-kvm9.inria.fr", fetch_list=True
            )
        )
    )
    vms = cloudstack.listVirtualMachines(
        hostid=host["id"], status="Running", listAll=True, fetch_list=True
    )
    vms_by_domain = collections.defaultdict(list)
    for vm in vms:
        vms_by_domain[vm["domainid"]].append(vm)
    for domainid, domain_vms in vms_by_domain.items():
        vm_names_comma = ", ".join(vm["name"] for vm in domain_vms)
        vm_list = "\n".join([f"- {vm['name']}" for vm in domain_vms])
        if len(domain_vms) == 1:
            vm_word = "virtual machine"
            vm_verb = "is running"
        else:
            vm_word = "virtual machines"
            vm_verb = "are running"
        smtp = smtplib.SMTP(host="smtp.inria.fr", port=25)
        smtp.ehlo()
        recipients = {
            user["email"]
            for user in cloudstack.listUsers(domainid=domainid, fetch_list=True)
        }
        recipients_string = ", ".join(recipients)
        domain = domain_vms[0]['domain']
        logging.info(f"{domain}: {recipients_string}")
        body = f"""Dear CI user,

We need to do some maintainance work on the machine where the following
{vm_word} {vm_verb}:
{vm_list}

Unfortunately, the {vm_word} cannot be moved during the maintainance.
We will have to shutdown and restart the {vm_word} during the operation,
which will occur on Tuesday 10 October, between 2pm and 2.30pm.

Sorry for the inconvenience.
-- 
Thierry, on behalf of the CI team.
"""
        msg = email.mime.text.MIMEText(body, "plain", "utf-8")
        msg[
            "Subject"
        ] = f"CI {vm_word} belonging to {domain} will be temporary stopped on Tuesday 10 October 2pm-2.30pm"
        msg["To"] = recipients_string
        msg["From"] = EMAIL_FROM
        smtp.send_message(msg, EMAIL_FROM, recipients)


if __name__ == "__main__":
    cloudstack = cs.CloudStack(**cs.read_config("portal"))
    main(cloudstack)
