import argparse
import collections
import contextlib
import typing

import ldap
import mysql.connector
import pydantic
import yaml

people_dn = "ou=people,dc=ci,dc=inria,dc=fr"
groups_dn = "ou=groups,dc=ci,dc=inria,dc=fr"
member_attr = "member"
mail_attr = "mail"


class Parameters(pydantic.BaseModel):
    database_host: str
    database_name: str
    database_user: str
    database_password: str
    ldap_host: str


def parse_parameters() -> Parameters:
    parser = argparse.ArgumentParser(prog="enumerate_jenkins_users")
    parser.add_argument("--parameters")
    args = parser.parse_args()
    parameters_path = args.parameters
    with open(parameters_path, "r") as file:
        parameters = Parameters(**yaml.safe_load(file)["parameters"])
    return parameters


def get_jenkins_projects(parameters: Parameters) -> typing.Iterable[str]:
    with contextlib.closing(
        mysql.connector.connect(
            user=parameters.database_user,
            password=parameters.database_password,
            host=parameters.database_host,
            database=parameters.database_name,
        )
    ) as cnx, contextlib.closing(cnx.cursor()) as cursor:
        cursor.execute("SELECT shortname FROM project WHERE software = 1")
        return [shortname for (shortname,) in cursor]


def get_project_users(
    parameters: Parameters,
    con: ldap.ldapobject.LDAPObject,
    project_shortname: str,
) -> typing.Optional[typing.Iterable[str]]:
    base_dn = f"ou={project_shortname},{groups_dn}"
    try:
        group_search_result = con.search_s(
            base_dn, ldap.SCOPE_SUBTREE, "(cn=ADMIN)", [member_attr]
        )
    except ldap.NO_SUCH_OBJECT:
        return None
    _dn, group_entry = next(iter(group_search_result))
    mails = []
    for member_dn in group_entry[member_attr]:
        member_dn = member_dn.decode("utf-8")
        mail_search_result = con.search_s(
            member_dn, ldap.SCOPE_BASE, f"(objectClass=*)", [mail_attr]
        )
        _dn, mail_entry = next(iter(mail_search_result))
        mails.extend(mail.decode("utf-8") for mail in mail_entry[mail_attr])
    return mails


def enumerate_jenkins_users(
    parameters: Parameters,
) -> typing.Mapping[str, typing.Iterable[str]]:
    con = ldap.initialize(parameters.ldap_host)
    con.simple_bind_s()
    users = collections.defaultdict(list)
    try:
        for project_shortname in get_jenkins_projects(parameters):
            project_users = get_project_users(parameters, con, project_shortname)
            if project_users:
                for user in project_users:
                    users[user].append(project_shortname)
    finally:
        con.unbind_s()
    return users


jenkins_users = enumerate_jenkins_users(parse_parameters())
for user, project_list in jenkins_users.items():
    projects = ", ".join(project_list)
    print(f"{user}: {projects}")
