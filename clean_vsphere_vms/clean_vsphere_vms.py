import argparse
import cs
import requests
import urllib3
import vmware.vapi.vsphere.client
import yaml


def connect_vsphere(config):
    session = requests.session()
    session.verify = False
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    return vmware.vapi.vsphere.client.create_vsphere_client(
        server="ci-vcenter.inria.fr", session=session, **config
    )


def connect_cloudstack(config):
    return cs.CloudStack(
        endpoint="https://sesi-cloud-ctl1.inria.fr/client/api", **config
    )


def get_cloudstack_vm_set(cloudstack):
    vms = cloudstack.listVirtualMachines(listAll=True, fetch_list=True)
    return {vm["instancename"] for vm in vms if vm["hypervisor"] == "VMware"}


def filter_vms(vsphere_client, vm_set, delete):
    for vm in vsphere_client.vcenter.VM.list():
        if vm.name not in vm_set:
            if delete:
                print(f"Deleting {vm}...")
                vsphere_client.vcenter.VM.delete(vm=vm.vm)
            else:
                print(vm)


def handle_commandline():
    parser = argparse.ArgumentParser(
        prog="clean_vsphere_vms",
        description="List VSphere VMs that are unknown by CloudStack.",
    )
    parser.add_argument("--delete", action="store_true", help="delete the VMs")
    args = parser.parse_args()
    config = yaml.safe_load(open("config.yml"))
    cloudstack = connect_cloudstack(config["cloudstack"])
    vm_set = get_cloudstack_vm_set(cloudstack)
    vsphere_client = connect_vsphere(config["vsphere"])
    filter_vms(vsphere_client, vm_set, args.delete)


if __name__ == "__main__":
    handle_commandline()
