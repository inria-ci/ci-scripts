The script [create-destroy-loop.sh] keeps creating and destroying the
virtual machine specified in [main.tf] in an infinite loop.

The script relies on the following environment variables.
Run `terraform init` first.

- for `qlf-ci`:
```
export CLOUDSTACK_API_URL=https://qlf-sesi-cloud-ctl1.inria.fr/client/api
export CLOUDSTACK_API_KEY=<...>
export CLOUDSTACK_SECRET_KEY=<...>
export TF_VAR_ZONE=qlf-zone-ci
export TF_VAR_CPU_SPEED=2000
```

- for `ci`:
```
export CLOUDSTACK_API_URL=https://sesi-cloud-ctl1.inria.fr/client/api
export CLOUDSTACK_API_KEY=<...>
export CLOUDSTACK_SECRET_KEY=<...>
export TF_VAR_ZONE=zone-ci
```
