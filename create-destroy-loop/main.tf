terraform {
  required_providers {
    cloudstack = {
      source  = "cloudstack/cloudstack"
      version = "0.4.0"
    }
  }
}

variable "ZONE" {
  type    = string
}

variable "CPU_SPEED" {
  type    = number
  default = null
}

provider "cloudstack" {
  ## Provided by environment
  # api_url    = "${var.cloudstack_api_url}"
  # api_key = "${var.cloudstack_api_key}"
  # secret_key = "${var.cloudstack_secret_key}"
}

resource "cloudstack_instance" "temporary-vm" {
  ## It is a good practice to have the "{project name}-" prefix
  ## in VM names.
  name             = "test-cloudstack-temporary-vm"
  service_offering = "Custom"
  template         = "ci.inria.fr/alpine-3.18.2/amd64"
  zone             = var.ZONE
  details = var.CPU_SPEED != null ? {
    cpuNumber = 1
    memory    = 2048
    cpuSpeed  = var.CPU_SPEED
  } : {
    cpuNumber = 1
    memory    = 2048
  }
  expunge = true
}
