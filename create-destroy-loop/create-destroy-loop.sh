while true
do
    terraform apply -var ZONE=qlf-zone-ci -var CPU_SPEED=2000 -auto-approve
    terraform destroy -var ZONE=qlf-zone-ci -var CPU_SPEED=2000 -auto-approve
done
