import requests
import urllib3
import vmware.vapi.vsphere.client
import yaml

def connect_vsphere(config):
    session = requests.session()
    session.verify = False
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    return vmware.vapi.vsphere.client.create_vsphere_client(server="ci-vcenter.inria.fr", username=config["username"], password=config["password"], session=session)

def fix_vm(vsphere_client, vm):
    sata_adapters = vsphere_client.vcenter.vm.hardware.adapter.Sata.list(vm=vm.vm)
    if sata_adapters == []:
        return
    scsi_adapters = vsphere_client.vcenter.vm.hardware.adapter.Scsi.list(vm=vm.vm)
    if scsi_adapters == []:
        vsphere_client.vcenter.vm.hardware.adapter.Scsi.create(vm=vm.vm, spec={})
    if vm.power_state == "POWERED_ON":
        vsphere_client.vcenter.vm.Power.stop(vm=vm.vm)
    disks = vsphere_client.vcenter.vm.hardware.Disk.list(vm=vm.vm)
    disk_infos = { disk.disk: vsphere_client.vcenter.vm.hardware.Disk.get(vm=vm.vm, disk=disk.disk) for disk in disks }
    for disk, disk_info in disk_infos.items():
        if disk_info.sata:
            vsphere_client.vcenter.vm.hardware.Disk.delete(vm=vm.vm, disk=disk)
            vsphere_client.vcenter.vm.hardware.Disk.create(vm=vm.vm, spec=dict(backing=dict(type="VMDK_FILE", vmdk_file=disk_info.backing.vmdk_file), type="SCSI"))
    for adapter in sata_adapters:
        vsphere_client.vcenter.vm.hardware.adapter.Sata.delete(vm=vm.vm, adapter=adapter.adapter)

config = yaml.safe_load(open("config.yml"))
vsphere_client = connect_vsphere(config["vsphere"])

#for vm in vsphere_client.vcenter.VM.list():
#    fix_vm(vsphere_client, vm)
