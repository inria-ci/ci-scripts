"""Check CI user addresses"""

import datetime
import typing
import yaml

import ldap
import mysql.connector


def read_yaml_file(filename: str):
    """read a file and return YAML contents"""
    with open(filename) as yaml_file:
        return yaml.load(yaml_file, Loader=yaml.FullLoader)


class MySQLCredentials(typing.NamedTuple):
    """credentials for MySQL"""

    host: str
    database: str
    user: str
    password: str

    @staticmethod
    def create_from_parameters(parameters):
        """extract MySQL credentials from parameters.yml"""
        return MySQLCredentials(
            host=parameters["database_host"],
            database=parameters["database_name"],
            user=parameters["database_user"],
            password=parameters["database_password"],
        )


class LDAPCredentials(typing.NamedTuple):
    """credentials for LDAP"""

    host: str
    user: str
    password: str

    @staticmethod
    def create_from_parameters(parameters):
        """extract LDAP credentials from parameters.yml"""
        return LDAPCredentials(
            host=parameters["ldap_host"],
            user=parameters["ldap_user"],
            password=parameters["ldap_password"],
        )


def get_ci_users(credentials: LDAPCredentials):
    """return the list of CI users (uid and email addresses)"""
    ldap_ci = ldap.initialize(credentials.host)
    ldap_ci.simple_bind_s(credentials.user, credentials.password)
    # pylint: disable=no-member
    users = ldap_ci.search_s(
        "ou=people,dc=ci,dc=inria,dc=fr", ldap.SCOPE_SUBTREE
    )
    return {
        user_infos["uid"][0]
        .decode("utf-8"): user_infos["mail"][0]
        .decode("utf-8")
        .lower()
        for (userdn, user_infos) in users
        if "mail" in user_infos
    }


def get_rerouting_addresses():
    """return dictionary associating Inria addresses to rerouting addresses,
    with empty strings when the account is still valid."""
    ldap_inria = ldap.initialize("ldaps://ildap.inria.fr")
    # pylint: disable=no-member
    users = ldap_inria.search_s(
        "ou=people,dc=inria,dc=fr", ldap.SCOPE_SUBTREE, "inriaEntryStatus=valid"
    )
    inria_emails = set(
        email.decode("utf-8").lower()
        for (uid, user_infos) in users
        for email in user_infos.get("mail", [])
    )
    emails = ldap_inria.search_s(
        "ou=service-mail,dc=inria,dc=fr", ldap.SCOPE_SUBTREE
    )
    result = {}
    for (_userdn, user_infos) in emails:
        principal_email = user_infos.get("inriaMail", None)
        if principal_email is None:
            continue
        principal_email = principal_email[0].decode("utf-8").lower()
        if principal_email in inria_emails:
            target = ""
        else:
            target = user_infos.get("mailRoutingAddress", None)
            if target is None:
                continue
            target = target[0].decode("utf-8").lower()
            if target.endswith("@zimbra.inria.fr"):
                continue
        result[principal_email] = target
        for alias in user_infos.get("mailLocalAddress", []):
            result[alias.decode("utf-8").lower()] = target
    return result


def connect_database(credentials):
    """connect to MySQL database"""
    return mysql.connector.connect(
        user=credentials.user,
        password=credentials.password,
        host=credentials.host,
        database=credentials.database,
    )


def get_user_last_activity(database) -> typing.Mapping[str, datetime.datetime]:
    """get date of user last activity"""
    cursor = database.cursor(named_tuple=True)
    cursor.execute(
        "select t1.userDN, t1.date from logging t1 "
        "join (select max(id) id from logging group by userDN) t2 "
        "where t1.id=t2.id "
    )
    return {row.userDN: row.date for row in cursor}


def execute(parameters):
    """check if all inria/irisa/loria CI users have valid email address"""

    database = connect_database(
        MySQLCredentials.create_from_parameters(parameters)
    )
    last_dates = get_user_last_activity(database)
    ldap_ci = get_ci_users(LDAPCredentials.create_from_parameters(parameters))
    rerouting_addresses = get_rerouting_addresses()
    recent_date = datetime.datetime.now() - datetime.timedelta(days=365)
    for login, email in ldap_ci.items():
        last_date = last_dates.get(
            f"uid={login},ou=people,dc=ci,dc=inria,dc=fr", None
        )
        if email.endswith(("@inria.fr", "@irisa.fr", "@loria.fr")):
            try:
                target = rerouting_addresses[email]
            except KeyError:
                target = "?"
            if target == "" or target.endswith("@sympa.inria.fr"):
                continue
        elif last_date is not None and last_date > recent_date:
            continue
        else:
            target = "(guest)"
        if last_date is None:
            last_date_str = ""
        else:
            last_date_str = last_date.isoformat()
        print(f"{email} -> {target} ({last_date_str})")


execute(
    read_yaml_file("/net/www/ci.inria.fr/shared/app/config/parameters.yml")[
        "parameters"
    ]
)
