#!/bin/bash
for file in /etc/systemd/system/jenk_*.service; do
    if ! grep '^ExecStart=.* --extractedFilesFolder=/tmp' $file; then
        sed -i.bak '/^ExecStart=/s|$| --extractedFilesFolder=/tmp|' $file
    fi
done

# for ((i = 1; i <= 16; i++)); do scp patch_jenk_service.sh ci-jenkins$i: && ssh ci-jenkins$i sudo sh patch_jenk_service.sh; done
