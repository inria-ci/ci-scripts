#!/bin/bash
set -ex
for ((i = 1; i <= 16; i++)); do
    scp remove_ajp13Port_option.sh ci-jenkins$i:
    ssh ci-jenkins$i sudo sh -c './remove_ajp13Port_option.sh'
done
