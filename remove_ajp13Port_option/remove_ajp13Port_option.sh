#!/bin/bash
set -ex
sed -i.bak 's/--ajp13Port=-1 //' /etc/systemd/system/jenk_*.service
systemctl daemon-reload
