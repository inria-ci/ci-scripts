#!/bin/bash
set -ex

admin_email=thierry.martinez@inria.fr

for project_name in $(ci project list software=jenkins short_name); do
    added=""
    if ! (ci member list "$project_name" email | grep "$admin_email"); then
        ci member add "$project_name" "$admin_email" --admin
        added=yes
    fi
    ci jenkins safe-restart "$project_name"
    if [ -n "$added" ]; then
        ci member delete "$project_name" "$admin_email"
    fi
done
