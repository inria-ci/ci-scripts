"""Purge pending users.

Suppose ~/.ci_credentials.yml contains the credentials for ci.inria.fr
(admin account needed).
"""

import logging
from lxml import etree
from io import StringIO
import os

import requests
import yaml

CI_URL = "https://ci.inria.fr"

# Set how many users we keep (for bug witness)
KEPT_COUNT = 10


def parse_html(response: requests.Response) -> etree._ElementTree:
    "Return the DOM of a given requests response."
    return etree.parse(
        StringIO(response.content.decode("utf-8")), parser=etree.HTMLParser()
    )


def login(session: requests.Session, username: str, password: str) -> None:
    "Log into ci.inria.fr in the given session with the given credentials."
    response = session.get(f"{CI_URL}/login")
    data = {"_username": username, "_password": password}
    # Fill CSRF token
    tree = parse_html(response)
    for input_elt in tree.xpath("//input[@type='hidden']"):
        data[input_elt.attrib["name"]] = input_elt.attrib["value"]
    response = session.post(f"{CI_URL}/login_check", data=data)
    if response.url != f"{CI_URL}/dashboard":
        raise Exception("Unable to login. Invalid credentials?")


def purge_pending_users(session: requests.Session) -> None:
    "Purge pending users in the given session."
    response = session.get(f"{CI_URL}/users/pendings")
    tree = parse_html(response)
    for path in tree.xpath(
        f"(//a[@name='preDeleteUser']/@data-email)[position()>{KEPT_COUNT}]"
    ):
        logging.info(path)
        try:
            session.get(f"{CI_URL}{path}")
        except requests.exceptions.HTTPError:
            # If deleting a user failed (e.g., if the user is `/etc/passwd/`),
            # the exception is logged and the execution continues.
            logging.exception(f"Failed to delete user: {path}")


def read_credentials_and_purge_pending_users() -> None:
    "Read user credentials and purge pending users."
    with open(os.path.join(os.environ["HOME"], ".ci_credentials.yml")) as f:
        credentials_file = yaml.safe_load(f)
    credentials = credentials_file["ci.inria.fr"]
    session = requests.session()
    # Call raise_for_status on each request
    # https://stackoverflow.com/questions/45470226/requests-always-call-raise-for-status
    session.hooks = {"response": lambda r, *args, **kwargs: r.raise_for_status()}
    login(session, credentials["username"], credentials["password"])
    purge_pending_users(session)


read_credentials_and_purge_pending_users()
