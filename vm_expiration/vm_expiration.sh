#!/bin/bash
set -e
BASE="/opt"
SCRIPT_DIR="$BASE/ci-scripts/vm_expiration/"
PARAMETERS=/net/www/qlf-ci.inria.fr/shared/app/config/parameters.yml
SCRIPT_FILE="$SCRIPT_DIR"/vm_expiration.py
PYTHON="$BASE/miniconda3/envs/vm_expiration/bin/python"

if [ "$(whoami)" = www-data ]; then
    "$PYTHON" "$SCRIPT_FILE" --parameters "$PARAMETERS" --timeout 60 "$@"
else
    sudo -u www-data "$PYTHON" "$SCRIPT_FILE" --parameters "$PARAMETERS" --timeout 60 "$@"
fi
