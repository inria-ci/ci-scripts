#!/bin/sh
set -ex
BASE="/opt"
MINICONDA_DIR="$BASE/miniconda3"
SCRIPT_DIR="$BASE/ci-scripts"
if [ ! -f Miniconda3-latest-Linux-x86_64.sh ]; then
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
fi
sha256sum --check <<EOF
1314b90489f154602fd794accfc90446111514a5a72fe1f71ab83e07de9504a7  Miniconda3-latest-Linux-x86_64.sh
EOF
sudo mkdir "$MINICONDA_DIR"
sudo chown www-data:ciadmins "$MINICONDA_DIR"
sudo chmod g+w "$MINICONDA_DIR"
sh Miniconda3-latest-Linux-x86_64.sh -b -u -p "$MINICONDA_DIR"
sudo mkdir "$SCRIPT_DIR"
sudo chown www-data:ciadmins "$SCRIPT_DIR"
sudo chmod g+w "$SCRIPT_DIR"
git clone git@gitlab.inria.fr:inria-ci/ci-scripts.git "$SCRIPT_DIR"
sudo chown -R www-data:ciadmins "$SCRIPT_DIR"
sudo chmod -R g+w "$SCRIPT_DIR"
# shellcheck source=/dev/null
. "$MINICONDA_DIR/etc/profile.d/conda.sh"
conda env create -f "$SCRIPT_DIR/vm_expiration/environment.yml"
sudo -u www-data crontab "$SCRIPT_DIR/vm_expiration/crontab"
