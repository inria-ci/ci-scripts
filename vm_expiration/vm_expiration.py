#!/usr/bin/env python

"""Check expiration dates for CI virtual machines, destroy expired machines and
notify users.

See README.md for usage.
"""

import argparse
import datetime
import email.mime.text
import logging
import smtplib
import sys
import textwrap
import typing
import yaml

import cs

# Interval between every warning mail
WARNING_INTERVAL = datetime.timedelta(days=7)

# Total number of warning emails before destroying the machine (the first email
# will thus be sent `WARNING_INTERNAL * DEFAULT_WARNING_COUNT` before machine
# destruction).
DEFAULT_WARNING_COUNT = 3

# The expiration date is stored as a tag associated to the VM with the
# key EXPIRATION_DATE_KEY, and the value is ISO 8601 formatted.  If
# there is no such field or if the field does not contain a valid date
# or if the date is too far in the future, the expirationDate is
# (re)initialized to the maximum possible value.
EXPIRATION_DATE_KEY = "expirationDate"

# The number of warning emails to send before destruction is stored in this
# tag associated to the VM. If there is no such field or if the field does not
# contain a valid positive integer, it is considered as DEFAULT_WARNING_COUNT.
REMAINING_WARNING_COUNT_KEY = "remainingWarningCount"

EMAIL_FROM = "ci-staff@inria.fr"

WARNING_EMAIL = {
    "subject": "Inria CI project {domain}: VM expires in {days} day{s}, {displayname}",
    "body": textwrap.dedent(
        """
        Dear CI user,

        The VM {displayname} in the CI project {domain} is about to expire
        ({destroy_date}) and will be destroyed in {days} day{s}.

        If you still want to use this VM, you may renew this expiration date
        with the following link:

        https://ci.inria.fr/project/{domain}/cloudService/CLOUDSTACK/slave/{vm_id}/prolongation

        The list of all the VMs associated to the project {domain} is
        available here, including expiration dates and the possibility to
        extend them:
        https://ci.inria.fr/project/{domain}/slaves

        Cheers,
        --
        The CI service.
        """
    ),
}

EXPIRE_EMAIL = {
    "subject": "Inria CI project {domain}: VM destroyed {displayname}",
    "body": textwrap.dedent(
        """
        Dear CI user,

        The VM {displayname} in the CI project {domain} has expired and has
        been destroyed.

        The VM will automatically be expunged in 24h. If you want to recover it
        before expunging, you may contact the CI administrators by answering
        this email.

        Cheers,
        --
        The CI service.
        """
    ),
}

MAX_EMAIL_COLUMN = 70


def get_vm_tag(cloudstack, vm_id: str, key: str) -> typing.Optional[str]:
    """Get the value for the tag key associated to the VM, if any."""
    response = cloudstack.listTags(
        resourceId=vm_id, resourceType="UserVm", key=key, listall=True
    )
    try:
        tags = response["tag"]
    except KeyError:
        return None
    return tags[0]["value"]


def delete_vm_tags(cloudstack, vm_id: str, *keys: str) -> None:
    """Delete tags associated to the VM."""
    cloudstack.deleteTags(
        resourceIds=vm_id,
        resourceType="UserVm",
        **{f"tags[{i}].key": key for (i, key) in enumerate(keys)},
    )


def set_vm_tags(cloudstack, vm_id: str, **tags: str) -> None:
    """Set tags associated to the VM."""
    cloudstack.createTags(
        resourceIds=vm_id,
        resourceType="UserVm",
        **{
            key: value
            for (i, (tag_key, tag_value)) in enumerate(tags.items())
            for (key, value) in [
                (f"tags[{i}].key", tag_key),
                (f"tags[{i}].value", tag_value),
            ]
        },
    )


def replace_vm_tags(cloudstack, vm_id: str, **tags: str) -> None:
    """Replace tags associated to the VM."""
    delete_vm_tags(cloudstack, vm_id, *tags.keys())
    set_vm_tags(cloudstack, vm_id, **tags)


def send_email(
    cloudstack,
    email_template,
    machine: typing.Mapping[str, str],
    expiration_date,
) -> None:
    """Send an email derived from `email_template` based on the
    informations of the dictionary `machine` describing a virtual
    machine. The `cloudstack` instance is used to query the emails of
    the project users.
    """
    domainid = machine["domainid"]
    domain = machine["domain"]
    days = (expiration_date - datetime.datetime.now()).days + 1
    variables = {
        "displayname": machine["displayname"],
        "domain": domain,
        "vm_id": machine["id"],
        "destroy_date": expiration_date.strftime("%A %d %B"),
        "days": days,
        "s": "" if days == 1 else "s",
    }
    subject = email_template["subject"].format(**variables)
    body, signature, = (
        email_template["body"].format(**variables).strip().split("--\n")
    )
    body = (
        "\n\n".join(
            [
                (
                    line
                    if "https:" in line
                    else "\n".join(textwrap.wrap(line, MAX_EMAIL_COLUMN))
                )
                for line in body.split("\n\n")
            ]
        )
        + "\n-- \n"
        + signature
    )
    try:
        users = cloudstack.listUsers(domainid=domainid)["user"]
    except KeyError:
        logging.info(f"No users for project {domain}")
        return
    recipients = set(user["email"] for user in users if "email" in user)
    recipients.discard("nobody@inria.fr")
    if not recipients:
        logging.info(f"No users for project {domain}")
        return
    recipients_string = ", ".join(recipients)
    logging.info("Send email to %s", recipients_string)
    smtp = smtplib.SMTP(host="smtp.inria.fr", port=25)
    smtp.ehlo()
    msg = email.mime.text.MIMEText(body, "plain", "utf-8")
    msg["Subject"] = subject
    msg["To"] = recipients_string
    msg["From"] = EMAIL_FROM
    smtp.send_message(msg, EMAIL_FROM, recipients)


def set_vm_expiration_date(cloudstack, vm_id, date, replace=False):
    """Set VM expiration date"""
    tags = {EXPIRATION_DATE_KEY: datetime.datetime.isoformat(date)}
    if replace:
        replace_vm_tags(cloudstack, vm_id, **tags)
    else:
        set_vm_tags(cloudstack, vm_id, **tags)


def parse_vm_expiration_date(
    txt: typing.Optional[str]
) -> typing.Optional[datetime.datetime]:
    """Return date from string txt if valid ISO-8601 date, None otherwise."""
    if txt is None:
        return None
    try:
        return datetime.datetime.fromisoformat(txt)
    except ValueError:
        return None


def check_vm(
    cloudstack,
    expiration_day_count: int,
    machine: typing.Mapping[str, typing.Any],
):
    """Check if the virtual machine described in the dictionary `machine` is
    not expired.  If necessary, destroy the virtual machine and notify
    or warn the users by email.
    """
    if machine["domain"] == "ROOT":
        return
    vm_id = machine["id"]
    tags = {tag["key"]: tag["value"] for tag in machine["tags"]}
    expiration_date = parse_vm_expiration_date(
        tags.get(EXPIRATION_DATE_KEY, None)
    )
    now = datetime.datetime.now()
    max_date = now + datetime.timedelta(days=expiration_day_count)
    logging.debug("Expiration date for %s: %s", vm_id, expiration_date)
    # If the date is invalid, reinitialize it
    if expiration_date is None or expiration_date > max_date:
        logging.info("(Re)initialize expiration date for %s", vm_id)
        # Forget previous warnings
        if EXPIRATION_DATE_KEY in tags or REMAINING_WARNING_COUNT_KEY in tags:
            delete_vm_tags(
                cloudstack,
                vm_id,
                EXPIRATION_DATE_KEY,
                REMAINING_WARNING_COUNT_KEY,
            )
        set_vm_expiration_date(cloudstack, vm_id, max_date)
        return
    # If the VM has expired, destroy it
    if expiration_date < now:
        logging.info("Destroy %s", vm_id)
        # Forget tags (in case the VM is restored)
        delete_vm_tags(
            cloudstack, vm_id, EXPIRATION_DATE_KEY, REMAINING_WARNING_COUNT_KEY
        )
        cloudstack.destroyVirtualMachine(id=vm_id)
        send_email(cloudstack, EXPIRE_EMAIL, machine, expiration_date)
        return
    delay_before_expiration = expiration_date - now
    # If it's time to send a warning, send it.
    # It is time to send a warning when the delay before the destruction
    # is less than the delay planned to send all the warning mails left
    # to be send.
    try:
        warning_count = int(tags[REMAINING_WARNING_COUNT_KEY])
    except (KeyError, ValueError):
        warning_count = DEFAULT_WARNING_COUNT
    if delay_before_expiration < WARNING_INTERVAL * warning_count:
        remaining_warning_count = delay_before_expiration // WARNING_INTERVAL
        logging.info(
            "Send warning (total: %d, left: %d) for %s",
            warning_count,
            remaining_warning_count,
            vm_id,
        )
        send_email(cloudstack, WARNING_EMAIL, machine, expiration_date)
        replace_vm_tags(
            cloudstack,
            vm_id,
            **{REMAINING_WARNING_COUNT_KEY: str(remaining_warning_count)},
        )
        return
    # If the project is no longer about to expire and some warnings have already
    # been sent, forget them.
    if (
        delay_before_expiration > WARNING_INTERVAL * DEFAULT_WARNING_COUNT
        and REMAINING_WARNING_COUNT_KEY in tags
    ):
        logging.info("Pop warning count for %s", vm_id)
        delete_vm_tags(cloudstack, vm_id, REMAINING_WARNING_COUNT_KEY)


def get_vm_infos(cloudstack, vm_id):
    """Return virtual machine informations"""
    return cloudstack.listVirtualMachines(id=vm_id)["virtualmachine"][0]


def check(cloudstack, expiration_day_count: int):
    """Check all the virtual machines of the `cloudstack` instance."""
    machines = cloudstack.listVirtualMachines(
        fetch_list=True, listAll=True, state="Present"
    )
    logging.info("Start checking all VMs")
    for machine in machines:
        try:
            check_vm(cloudstack, expiration_day_count, machine)
        except cs.client.CloudStackApiException as exc:
            logging.info("Error on %s: %s", machine["id"], exc)
    logging.info("End checking all VMs")


def read_config_from_parameters_yml(path):
    """Read CloudStack configuration from the `parameters.yml` file given
    in `path`."""
    with open(path) as yaml_file:
        parameters = yaml.load(yaml_file, Loader=yaml.FullLoader)["parameters"]
    cloudstack_parameters = parameters["cloudstack_parameters"]
    config = {
        "endpoint": cloudstack_parameters["cs_api_endpoint"],
        "key": cloudstack_parameters["cs_api_key"],
        "secret": cloudstack_parameters["cs_secret_key"],
    }
    expiration_day_count = parameters["cloud_parameters"][
        "vm_expiration_day_count"
    ]
    return (config, expiration_day_count)


def update_vm_details(cloudstack, vm_id, update):
    """Apply in-place `update` function to VM details."""
    machine = get_vm_infos(cloudstack, vm_id)
    details = machine["details"]
    update(details)
    cloudstack.updateVirtualMachine(id=vm_id, details=details)


def get_maximum_vm_date(expiration_day_count) -> datetime.datetime:
    """Return the maximum date allowed for a given VM."""
    now = datetime.datetime.now()
    return now + datetime.timedelta(days=expiration_day_count)


def reset_vm_date(cloudstack, expiration_day_count, vm_id) -> datetime.datetime:
    """Reset expiration date to the maximum allowed."""
    max_date = get_maximum_vm_date(expiration_day_count)
    set_vm_expiration_date(cloudstack, vm_id, max_date, replace=True)
    return max_date


def reset_all_vm_dates(cloudstack, expiration_day_count) -> datetime.datetime:
    """Reset expiration date to the maximum allowed."""
    max_date = get_maximum_vm_date(expiration_day_count)
    machines = cloudstack.listVirtualMachines(
        fetch_list=True, listAll=True, state="Present"
    )
    logging.info("Start resetting all VMs")
    for machine in machines:
        vm_id = machine["id"]
        logging.info("Resetting %s", vm_id)
        set_vm_expiration_date(cloudstack, vm_id, max_date, replace=True)
    logging.info("End resetting all VMs")
    return max_date


class Error(Exception):
    """Exception for command-line error message"""


def parse_commandline() -> None:
    """Parse command-line arguments"""
    parser = argparse.ArgumentParser(description="Manage VM expiration dates")
    parser.add_argument(
        "--parameters", type=str, help="path to `parameters.yml`"
    )
    parser.add_argument("--get", nargs=1, help="print expiration date")
    parser.add_argument("--delete", nargs=1, help="delete expiration date")
    parser.add_argument("--set", nargs=2, help="set expiration date")
    parser.add_argument("--reset", nargs=1, help="reset expiration date")
    parser.add_argument(
        "--reset-all", action="store_true", help="reset all expiration dates"
    )
    parser.add_argument(
        "--loglevel",
        default="INFO",
        help="log level: WARNING, INFO (default), DEBUG",
    )
    parser.add_argument(
        "--timeout",
        help="set CloudStack timeout"
    )
    args = parser.parse_args()
    logging.basicConfig(
        level=args.loglevel, format="%(asctime)-15s %(message)s"
    )
    if args.parameters:
        (config, expiration_day_count) = read_config_from_parameters_yml(
            args.parameters
        )
    else:
        config = cs.read_config()
        expiration_day_count = None
    if args.timeout:
        config["timeout"] = args.timeout
    cloudstack = cs.CloudStack(**config)
    if args.get:
        date = parse_vm_expiration_date(
            get_vm_tag(cloudstack, args.get, EXPIRATION_DATE_KEY)
        )
        if date is None:
            raise Error("No date set for the VM.")
        print(datetime.datetime.isoformat(date))
        return
    if args.delete:
        delete_vm_tags(cloudstack, args.delete, EXPIRATION_DATE_KEY)
        return
    if args.set:
        date = parse_vm_expiration_date(args.set[1])
        set_vm_expiration_date(cloudstack, args.set[0], date, replace=True)
        return
    if expiration_day_count is None:
        raise Error("parameters.yml is requered for expiration delay")
    if args.reset:
        reset_vm_date(cloudstack, expiration_day_count, args.reset)
        return
    if args.reset_all:
        reset_all_vm_dates(cloudstack, expiration_day_count)
        return
    check(cloudstack, expiration_day_count)


try:
    parse_commandline()
except Error as exc:
    print(exc, file=sys.stderr)
    sys.exit(1)
