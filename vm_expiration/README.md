# Management script for expiration dates of CI virtual machines

## Usage

The conda environment `expiration` may be initialized with the
following commands.

```
conda env create -f environment.yml
conda activate expiration
```

The script either takes an argument `--parameters` with the path to
`parameters.yml` (should be run on `ci` or `qlf-ci` as `www-data` to
have the correct permissions) or use the usual CloudStack settings
(typically, a `./cloudstack.ini` file, see
https://github.com/exoscale/cs). `parameters.yml` is required for
`--reset` or when used with no options since time limits for each
hypervisor are defined there.

With the argument `--get VM-ID`, returns (prints) the expiration date
associated to the given machine.

With the argument `--set VM-ID DATE`, sets the expiration date
associated to the given machine. `DATE` should be ISO-8601 formatted.

With the argument `--delete VM-ID`, deletes the expiration date
associated to the given machine.

With the argument `--reset VM-ID`, sets the expiration date
to the maximum allowed.

By default, if there is none of the four arguments `--get`, `--set`,
`--delete`, `--reset`, the script checks all the VMs, (re-)initializes
the dates that are unset or incorrect, warns the users of the VMs that
are about to expire, and destroys the VMs that are expired (and
notifies the users).

## Expiration date storage

Expiration dates are stored in the key/value dictionary `details` in
VM fields. The key is `expirationDate` and the value is the ISO-8601
formatted date.

## Expiration date rules

As the time of writing (2021-02-15), the expiration date for a given
VM cannot exceed:

- 365 days (one year) from current date for a VM hosted by the `KVM`
  hypervisor,

- 31 days (one month) from current date for a VM hosted by the
  `VMware` hypervisor.

If an expiration date exceeds these limits, it is considered as
incorrect and reset to the maximum allowed value.

If a VM is about to expire (within 7 days), a warning email is sent
each day to the users associated to the VM project.
