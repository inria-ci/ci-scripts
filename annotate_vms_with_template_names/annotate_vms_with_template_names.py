import cs
import logging

# Setup logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def has_annotation(vm_id):
    """Check if a VM already has an annotation."""
    annotations = client.listAnnotations(entityType="HOST", entityId=vm_id, fetch_list=True)
    return len(annotations) > 0

def annotate_vm_with_template(vm_id, template_name):
    """Annotate a VM with its template name."""
    if not has_annotation(vm_id):
        client.addAnnotation(
            entityType="HOST", 
            entityId=vm_id,
            entityUuid=vm_id,
            annotation=template_name
        )
        logging.info(f"Annotated VM {vm_id} with template name {template_name}")
    else:
        logging.info(f"VM {vm_id} already has an annotation. Skipping.")

def main(client):
    # Fetch all VMs
    virtual_machines = client.listVirtualMachines(fetch_list=True, listAll=True)

    for vm in virtual_machines:
        vm_id = vm['id']
        template_name = vm.get('templatename', None)  # Directly extract the template name
        
        if template_name:
            # Annotate the VM with the template name
            annotate_vm_with_template(vm_id, template_name)
        else:
            logging.warning(f"Could not find template name for VM {vm_id}")

if __name__ == "__main__":
    # Initialize the Exoscale API client
    config = cs.read_config("portal")
    config["timeout"] = 60
    client = cs.CloudStack(**config)
    main(client)
